from django.contrib import admin
from .models import Company, Product, Category, Order, Discount, CustomersEmail, Comment, OrderProduct, ProductVariety


class ProductVarietyInline(admin.TabularInline):
    model = ProductVariety
    extra = 0
    raw_id_fields = ['product']


@admin.register(ProductVariety)
class ProductVarietyAdmin(admin.ModelAdmin):
    list_display = ['id', 'product', 'variety', 'weight', 'price']
    list_display_links = ['id', 'product', 'variety']


class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'iid', 'title', 'company', 'description', 'price', 'get_categories', 'image']
    list_display_links = ['title']
    readonly_fields = ['image_tag']
    inlines = [ProductVarietyInline]


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['company_name', 'company_email', 'company_phone', 'company_website', 'company_address', 'image']
    list_display_links = ['company_name']
    readonly_fields = ['image_tag']


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id', 'category_name', 'get_products']
    list_display_links = ['category_name']
    readonly_fields = ('get_products',)


class OrderProductInline(admin.TabularInline):
    model = OrderProduct
    extra = 1
    raw_id_fields = ['product']


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'customer_name', 'phone_number', 'address', 'registration_date']
    list_display_links = ['id', 'customer_name']
    inlines = [OrderProductInline]
    readonly_fields = ['comments', 'get_cost', 'get_delivery_fee', 'get_total_cost']
    search_fields = ['id', 'phone_number']
    exclude = ['cost', 'fee', 'total_cost']


class DiscountAdmin(admin.ModelAdmin):
    list_display = ['company', 'description', 'is_relevant', 'end_time']
    list_display_links = ['company', 'description']
    readonly_fields = ['image_tag']


class EmailAdmin(admin.ModelAdmin):
    list_display = ['email']
    list_display_links = ['email']


class CommentAdmin(admin.ModelAdmin):
    list_display = ['author', 'body', 'company', 'rating', 'created', 'active']
    list_display_links = ['author']


admin.site.register(Company, CompanyAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Discount, DiscountAdmin)
admin.site.register(CustomersEmail, EmailAdmin)
admin.site.register(Comment, CommentAdmin)

