from rest_framework import serializers
from ..models import Company, Product, Category, Order, Discount, Comment, CustomersEmail, OrderProduct


# Serializers define the API representation.
class ProductSerializer(serializers.HyperlinkedModelSerializer):
    company = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = Product
        fields = ['id', 'iid', 'company', 'title', 'description', 'price', 'image', 'registration_date']


class CategorySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Category
        fields = ['url', 'category_name']


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    categories = CategorySerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = ['id', 'company_name', 'company_email', 'company_phone', 'company_website', 'company_address',
                  'fixed_fee', 'rating', 'description', 'image', 'registration_date', 'categories']


class SearchCompaniesSerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = ['company_name', 'delivery_time', 'rating', 'fixed_fee', 'product']
        # fields = '__all__'


class CompanyProductSerializer(serializers.HyperlinkedModelSerializer):
    # TODO remove after company/{id} return products
    products = ProductSerializer(many=True)

    class Meta:
        model = Company
        fields = ['id', 'company_name', 'company_email', 'company_phone', 'company_website', 'company_address',
                  'fixed_fee', 'rating', 'description',
                  'image', 'registration_date', 'products']


class OrderProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderProduct
        fields = ['product', 'quantity']


class OrderSerializer(serializers.ModelSerializer):
    products = OrderProductSerializer(many=True)

    class Meta:
        model = Order
        fields = ['id', 'customer_name', 'phone_number', 'address', 'comments', 'products']

    def create(self, validated_data):
        products_data = validated_data.pop('products')
        order = Order.objects.create(**validated_data)
        for product_data in products_data:
            OrderProduct.objects.create(order=order, **product_data)
        return order


class DiscountSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Discount
        fields = ['company', 'description', 'image', 'end_time']


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Comment
        fields = ['author', 'company', 'body', 'rating', 'created', 'updated', 'active']


class CustomerEmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomersEmail
        fields = ['id', 'email']

