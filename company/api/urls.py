from django.urls import include, path
from rest_framework import routers
from . import views

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register('company', views.CompanyViewSet, basename='company')
router.register('products', views.ProductViewSet, basename='product')
router.register('category', views.CategoryViewSet, basename='category')
router.register('order', views.OrderViewSet, basename='order',)
router.register('discount', views.DiscountViewSet, basename='discount')
router.register('discount-relevant', views.DiscountRelevantViewSet, basename='discount-relevant')
router.register('comment', views.CommentViewSet, basename='comment')
router.register('email', views.CustomersEmailViewSet, basename='email')
# router.register('search', views.search, basename='search')

urlpatterns = [
    path('api/', include(router.urls)),
    path('search/', views.search, name='search')
]