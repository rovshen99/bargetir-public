from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes

from ..models import Company, Product, Category, Order, Discount, Comment, CustomersEmail
from .serializers import CompanySerializer, CompanyProductSerializer, ProductSerializer, CategorySerializer,\
    OrderSerializer, DiscountSerializer, CommentSerializer, CustomerEmailSerializer, SearchCompaniesSerializer


def index(request):
    return HttpResponse("Hello, world. You're at the COMPANIE's index.")


# ViewSets define the view behavior.
class CompanyViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving companies.
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def list(self, request):
        category_list = self.request.GET.getlist('categories', None)
        if category_list:
            queryset = Company.objects.none()
            for category in category_list:
                queryset |= Company.objects.all().filter(product__category__id=category).distinct()
        else:
            queryset = Company.objects.all().order_by('registration_date')
        for company in queryset:
            company_products = company.product_set.all()
            products_categories = Category.objects.none()
            for product in company_products:
                product_category = product.category_set.all()
                products_categories |= product_category
            setattr(company, 'categories', set(products_categories))
        serializer = CompanySerializer(queryset, context={'request': request}, many=True)
        data = {
            'companies': serializer.data,
            'count': queryset.count()
        }
        return Response(data)

    def retrieve(self, request, pk=None):
        queryset = Company.objects.all()
        company = get_object_or_404(queryset, pk=pk)
        products = Product.objects.filter(company=company)
        setattr(company, 'products', products)
        serializer = CompanyProductSerializer(company, context={'request': request},)
        return Response(serializer.data)


class ProductViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving products.
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def list(self, request):
        queryset = Product.objects.all().order_by('registration_date')
        serializer = ProductSerializer(queryset, context={'request': request}, many=True)
        data = {
            'products': serializer.data,
            'count': queryset.count()
        }
        return Response(data)

    def retrieve(self, request, pk=None):
        queryset = Product.objects.all()
        product = get_object_or_404(queryset, pk=pk)
        serializer = ProductSerializer(product, context={'request': request},)
        return Response(serializer.data)


class CategoryViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving categories.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def list(self, request):
        queryset = Category.objects.all()
        serializer = CategorySerializer(queryset, context={'request': request}, many=True)
        data = {
            'categories': serializer.data,
            'count': queryset.count()
        }
        return Response(data)

    def retrieve(self, request, pk=None):
        queryset = Category.objects.all()
        category = get_object_or_404(queryset, pk=pk)
        serializer = CategorySerializer(category, context={'request': request},)
        return Response(serializer.data)


class OrderViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving orders.
    """

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [AllowAny]
    http_method_names = ['post']


class DiscountViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving discounts.
    """
    queryset = Discount.objects.all()
    serializer_class = DiscountSerializer

    def list(self, request):
        queryset = Discount.objects.all()
        serializer = DiscountSerializer(queryset, context={'request': request}, many=True)
        data = {
            'discounts': serializer.data,
            'count': queryset.count()
        }
        return Response(data)

    def retrieve(self, request, pk=None):
        queryset = Discount.objects.all()
        order = get_object_or_404(queryset, pk=pk)
        serializer = DiscountSerializer(order, context={'request': request},)
        return Response(serializer.data)


class DiscountRelevantViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving relevant discounts.
    """
    queryset = Discount.objects.all()
    serializer_class = DiscountSerializer

    def list(self, request):
        queryset = Discount.objects.all()
        relevants = []
        for discount in queryset:
            if discount.is_relevant():
                relevants.append(discount)
        serializer = DiscountSerializer(relevants, context={'request': request}, many=True)
        data = {
            'discounts': serializer.data,
            'count': len(relevants)
        }
        return Response(data)

    def retrieve(self, request, pk=None):
        queryset = Discount.objects.all()
        order = get_object_or_404(queryset, pk=pk)
        serializer = DiscountSerializer(order, context={'request': request},)
        return Response(serializer.data)


class CommentViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving Comment.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def list(self, request):
        queryset = Comment.objects.all()
        serializer = CommentSerializer(queryset, context={'request': request}, many=True)
        data = {
            'discounts': serializer.data,
            'count': queryset.count()
        }
        return Response(data)

    def retrieve(self, request, pk=None):
        queryset = Comment.objects.all()
        order = get_object_or_404(queryset, pk=pk)
        serializer = CommentSerializer(order, context={'request': request},)
        return Response(serializer.data)


class CustomersEmailViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for listing or retrieving relevant customer's email.
    """
    queryset = CustomersEmail.objects.all()
    serializer_class = CustomerEmailSerializer
    permission_classes = [AllowAny]


@api_view(['GET'])
@permission_classes((AllowAny,))
def search(request):
    q = request.query_params.get('q', '')
    if q:
        companies = Company.objects.filter(company_name__icontains=q)
        companies |= Company.objects.filter(product__title__icontains=q)
        for company in companies:
            product = company.product_set.filter(title__icontains=q)
            setattr(company, 'product', product)
        data = {
            'companies': SearchCompaniesSerializer(set(companies), context={'request': request}, many=True).data,
        }
        return Response(data)
    else:
        return Response({"companies": []})
