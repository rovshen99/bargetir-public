import uuid
import re
from datetime import datetime, timezone
from django.db import models
from django.utils.html import mark_safe
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from django.core.mail import send_mail
from django.core.validators import MinValueValidator
from django.template import loader
import os
from dotenv import load_dotenv
load_dotenv()


class Company(models.Model):
    Rating_CHOICES = (
        (1, 'Poor'),
        (2, 'Average'),
        (3, 'Good'),
        (4, 'Very Good'),
        (5, 'Excellent')
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    company_name = models.CharField(max_length=50)
    company_email = models.CharField(max_length=50)
    company_phone = models.CharField(max_length=50)
    company_website = models.CharField(max_length=50, blank=True, null=True)
    company_address = models.CharField(max_length=50)
    from_hour = models.TimeField(blank=True, null=True)
    to_hour = models.TimeField(blank=True, null=True)
    delivery_time = models.TimeField(blank=True, null=True)
    fixed_fee = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    rating = models.IntegerField(choices=Rating_CHOICES, blank=True, null=True)
    description = models.TextField(default='')
    image = models.ImageField(upload_to='companies_images', null=True, blank=True)
    registration_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

    def __str__(self):
        return self.company_name + " : " + self.id.urn[9:] + " : " + self.registration_date.strftime("%d/%m/%Y %H:%M:%S")

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="/media/%s" width="200" height="150" />' % self.image)

    def get_categories(self):
        return self.product_set.all().values('category__category_name').distinct()

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'
        db_table = 'company'


def user_directory_path(instance, filename):
    return '{0}'.format(filename)


class Product(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=75)
    price = models.DecimalField(max_digits=8, decimal_places=2)
    image = models.ImageField(upload_to='products_images', null=True, blank=True)
    iid = models.CharField(max_length=3, null=False, blank=False, default='000')
    registration_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

    def get_categories(self):
        return list(self.category_set.all())

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="/media/%s" width="150" height="130" />' % self.image)

    def __str__(self):
        return '{} {}'.format(self.title, self.company.company_name)

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        db_table = 'product'
        ordering = ('-registration_date',)


class ProductVariety(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    variety = models.CharField(max_length=60)
    weight = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=8, decimal_places=2)

    def __str__(self):
        return self.variety


@receiver(post_delete, sender=Product)
@receiver(post_delete, sender=Company)
def submission_delete(sender, instance, **kwargs):
    instance.image.delete(False)


@receiver(pre_save, sender=Company)
@receiver(pre_save, sender=Product)
def delete_file_on_change_extension(sender, instance, **kwargs):
    if sender.__name__ == 'Company':
        try:
            old_image = Company.objects.get(pk=instance.pk).image
        except Company.DoesNotExist:
            return
        else:
            new_image = instance.image
            if old_image and old_image.url != new_image.url:
                old_image.delete(save=False)
    elif sender.__name__ == 'Product':
        try:
            old_image = Product.objects.get(pk=instance.pk).image
        except Product.DoesNotExist:
            return
        else:
            new_image = instance.image
            if old_image and old_image.url != new_image.url:
                old_image.delete(save=False)


class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    category_name = models.CharField('Kategoriya', max_length=50,)
    product = models.ManyToManyField(Product, blank=True, null=True)
    registration_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

    def get_products(self):
        return list(self.product.all())

    def __str__(self):
        return self.category_name

    class Meta:
        db_table = 'category'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Order(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    customer_name = models.CharField('Imya kliyenta', max_length=50,)
    phone_number = models.CharField('Nomer telefona', max_length=20)
    address = models.CharField('Address', max_length=100)
    comments = models.TextField(blank=True, null=True, default='')
    registration_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    cost = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    fee = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    total_cost = models.DecimalField(max_digits=8, decimal_places=2, default=0)

    def __str__(self):
        return self.customer_name + ' ' + self.phone_number

    def get_cost(self):
        return sum(product.get_cost() for product in self.products.all())
    
    def get_delivery_fee(self):
        uniqueCompanies = []
        for product in self.products.all():
            if product.product.company not in uniqueCompanies:
                uniqueCompanies.append(product.product.company)
        return sum(company.fixed_fee for company in uniqueCompanies)

    def get_total_cost(self):
        return self.get_delivery_fee() + self.get_cost()

    def save(self, *args, **kwargs):
        self.cost = self.get_cost()
        self.fee = self.get_delivery_fee()
        self.total_cost = self.get_total_cost()
        super(Order, self).save(*args, **kwargs)

    class Meta:
        db_table = 'order'
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'
        ordering = ('-registration_date',)


class OrderProduct(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='products')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(validators=[
            MinValueValidator(1)
        ], default=1)

    def __str__(self):
        return str(self.id)
    
    def get_cost(self):
        return self.product.price * self.quantity


# def send_email(order_link):
#         html_message = loader.render_to_string('mail/new_order.html', {
#             'order_link': order_link,
#             })
#         send_mail('Zakaz Geldi', '', os.getenv('MAIL_SENDER_USERNAME'),
#                 [os.getenv('MAIL_RECIEVER_USERNAME')], fail_silently=False, html_message=html_message)
#
#
# def send_sms(phone_number, message):
#     aws_access_key_id = os.getenv('AWS_SMS_SENDER_ACCESS_KEY_ID')
#     aws_secret_access_key = os.getenv('AWS_SMS_SENDER_SECRET_ACCESS_KEY')
#     region_name = os.getenv('AWS_REGION')
#     client = boto3.client('sns', aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, region_name=region_name)
#     client.publish(PhoneNumber=phone_number, Message=message)


# @receiver(post_save, sender=Order)
# def resolve_post_order_save(sender, instance, created, **kwargs):
#     if created:
#         order = instance
#         hostname = os.getenv('APP_HOSTNAME')
#         port = os.getenv('APP_PORT')
#         order_link = f'https://{hostname}:{port}/admin/company/order/{str(order.id)}'
#         send_email(order_link)
#
#         phone_number_regex = r'^\+9936[1-5]\d{6}$'
#         company_sms_dict = {}
#         number_valid = re.match(phone_number_regex, order.phone_number)
#         if(number_valid):
#             for orderProduct in order.products.all():
#                 key = orderProduct.product.company.company_phone
#                 value = company_sms_dict.get(key)
#                 product_quantity = f'{orderProduct.product.iid}#{orderProduct.quantity}'
#                 if(value):
#                     value.append(product_quantity)
#                 else:
#                     value = [ product_quantity ]
#                 company_sms_dict.update({key: value})
#
#             for phone_number, values  in company_sms_dict.items():
#                 sms_message = "\n".join([
#                     f'BG {order.phone_number}',
#                     f'{order.customer_name[:20]}',
#                     f'{order.address[:20]}',
#                     '\n'.join(values)
#                 ])
#                 print('=================SMS BEGIN==================')
#                 print(phone_number)
#                 print(sms_message)
#                 # send_sms(phone_number, sms_message)
#                 print('=================SMS END==================')
#         else:
#             raise Exception('Invalid order phone number exception')


class Discount(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='discount_images', null=True, blank=True)
    description = models.TextField()
    end_time = models.DateTimeField(editable=True, blank=True, null=True)

    def __str__(self):
        return str(self.company) + ' ' + self.description

    def image_tag(self):
        if self.image:
            return mark_safe('<img src="/media/%s" width="200" height="150" />' % self.image)

    def is_relevant(self):
        if (self.end_time - datetime.now(timezone.utc)).total_seconds() > 0:
            return True

    class Meta:
        db_table = 'discount'
        verbose_name_plural = 'Акция'
        verbose_name = 'Акции'


class CustomersEmail(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField()

    def __str__(self):
        return self.email

    class Meta:
        db_table = 'email'
        verbose_name = 'Почта'
        verbose_name_plural = 'Почты'


class Comment(models.Model):
    Rating_CHOICES = (
        (1, 'Poor'),
        (2, 'Average'),
        (3, 'Good'),
        (4, 'Very Good'),
        (5, 'Excellent')
    )
    author = models.CharField(max_length=50)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    body = models.TextField()
    rating = models.IntegerField(choices=Rating_CHOICES, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.author + ': ' + str(self.company)

    class Meta:
        db_table = 'comment'
        ordering = ('created',)



