from rest_framework import permissions

class AllowWriteByIpPermission(permissions.BasePermission):
    """
    Global permission check for whitelisted IPs.
    """

    # def has_permission(self, request, view):
    #     ip_addr = request.META['REMOTE_ADDR']
    #     blacklisted = AllowWriteByIp.objects.filter(ip_addr=ip_addr).exists()
    #     return not blacklisted