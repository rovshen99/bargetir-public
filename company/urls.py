from django.urls import include, path

from .views import cart_api
urlpatterns = [
    path('cart-api/', cart_api, name='cart-api')
]