from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse


def cart_api(request, **kwargs):
    product = kwargs.get('product', "")
    quantity = kwargs.get('quantity', 1)
    total_value = product * quantity
    return JsonResponse({'total_value': total_value})
