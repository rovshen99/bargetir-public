import uuid
import datetime
from django.db import models

class Courier(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    registration_date = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
    def __str__(self):
        return self.name + " : " + self.id.urn[9:] + " : " + self.registration_date.strftime("%d/%m/%Y %H:%M:%S")
    class Meta:
            db_table = 'courier'
            managed = True